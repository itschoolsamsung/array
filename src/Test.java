import java.util.Arrays;
import java.util.Comparator;

public class Test {
    static void printA(int[] a) {
        if (null == a) {
            return;
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
//        for (int x : a) {
//            System.out.print(x + " ");
//        }
        System.out.println();
    }

    static void printB(int[][] b) {
        if (null == b) {
            return;
        }

        for (int[] row : b) {
            for (int x : row) {
                System.out.print(x + " ");
            }
            System.out.println();
        }
    }

    static void randA(int[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = (int)(Math.random() * 10);
        }
    }

    static void printI(Integer[] a) {
        if (null == a) {
            return;
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

    static void randI(Integer[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = (int)(Math.random() * 10);
        }
    }

    public static void main(String[] args) {
        int[] a1;
        int[] a2 = null;
        int[] a3 = {1, 2, 3};
        int[] a4 = new int[3];
        int[] a5 = new int[]{1, 2, 3};

        printA(a2);
        printA(a3);
        printA(a4);
        printA(a5);

        /*
         * clone
         */

        int[] a6 = {10, 20, 30};
        printA(a6);

        int[] a7 = a6;
        a6[0] = 11;
        printA(a7);

        int[] a8 = a6.clone();
        a6[0] = 12;
        printA(a8);

        int[] a9 = new int[a6.length];
        System.arraycopy(a6, 0, a9, 0, a6.length);
        a6[0] = 13;
        printA(a9);

        /*
         * clone x2
         */

        int[][] b1 = {
                {1, 2, 3},
                {4, 5, 6},
        };
        printB(b1);

        int[][] b2 = b1.clone();
        b1[0][0] = 10;
        printB(b2);

        int[][] b3 = {
                b1[0].clone(),
                b1[1].clone(),
        };
        b1[0][0] = 11;
        printB(b3);

        /*
         * arrays
         */

        int[] c1 = new int[10];
        Arrays.fill(c1, 1);
        printA(c1);
        Arrays.fill(c1, 3, 5, 0);
        printA(c1);

        int[] c2 = new int[10];
        randA(c2);
        printA(c2);
        Arrays.sort(c2);
        printA(c2);

        System.out.println(Arrays.equals(new int[]{1, 2, 3}, new int[]{1, 2, 3}));

        Comparator c = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 < o2) {
                    return 1;
                }
                if (o1 > o2) {
                    return -1;
                }
                return 0;
            }
        };

        Integer[] c3 = new Integer[10];
        randI(c3);
        printI(c3);
        Arrays.sort(c3, c);
        printI(c3);

        Arrays.sort(c3);
        printI(c3);
        System.out.println(Arrays.binarySearch(c3, 1));
    }
}
